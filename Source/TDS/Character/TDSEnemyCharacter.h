// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/Interface/TDSUIGameActor.h"
#include "TDS/Weapon/TDSStateEffect.h"
#include "TDSEnemyCharacter.generated.h"

UCLASS()
class TDS_API ATDSEnemyCharacter : public ACharacter, public ITDSUIGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDSEnemyCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	virtual void RemoveEffect(UTDSStateEffect* RemoveEffect) override;
	virtual void AddEffect(UTDSStateEffect* NewEffect) override;
	virtual void GetSpawmEffectInfo(FName& AttachedBone, FVector& Offset) override;

	UFUNCTION()
		void Notify_AddEffect();
	UFUNCTION()
		void Notify_RemoveEffect();
	UFUNCTION()
		void SwitchEffect(UTDSStateEffect* Effect, bool bShouldAdd);
	UFUNCTION(Server, Reliable)
		void Server_ExecuteNewEffect(UParticleSystem* Effect);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_ExecuteNewEffect(UParticleSystem* Effect);

public:

	// Effects
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Settings")
		TArray<UTDSStateEffect*> CurrentEffects;
	UPROPERTY(ReplicatedUsing = Notify_AddEffect)
		UTDSStateEffect* RepAddedEffect = nullptr;
	UPROPERTY(ReplicatedUsing = Notify_RemoveEffect)
		UTDSStateEffect* RepRevomedEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> CurrentParticleCompoponents;
};
