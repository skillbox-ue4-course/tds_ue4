// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS/FunctionLibrary/Types.h"
#include "TDSInventoryComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAddInfo, int32, WeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChanged, EWeaponType, WeaponType, int32, NewAmmoVal);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponClipLoadChanged, int32, WeaponIndex, int32, NewClipLoadVal);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvailable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnInventorySlotsUpdate, int32, SlotIndex, FWeaponSlot, SlotInfo);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDSInventoryComponent();

	FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoChanged OnAmmoChanged;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponClipLoadChanged OnWeaponClipLoadChanged;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAmmoAvailable OnWeaponAmmoAvailable;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnInventorySlotsUpdate OnInventorySlotsUpdate;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FAmmoSlot> AmmoSlots;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		int32 MaxSlotsNumber = 0;

	bool SwitchWeaponToNext(bool bForward, int32 PrevIndex, FAdditionalWeaponInfo PrevInfo);
	//bool SwitchWeaponToNext(bool bForward, int32 PrevIndex, FAdditionalWeaponInfo PrevInfo);
	int32 GetWeaponIndexByName(FName WeaponName) const;
	
	FName GetWeaponNameBySlotIndex(int32 IndexSlot) const;

	FAdditionalWeaponInfo GetAdditionalWeaponInfoByIndex(int32 IndexWeapon) const;
	void SetAdditionalWeaponInfoByIndex(int32 IndexWeapon, FAdditionalWeaponInfo AddInfo);
	
	UFUNCTION(BlueprintCallable)
	void ChangeAmmoSlotValue(EWeaponType Weapon, int32 AmmoDiff);

	int32 GetAvailableAmmoForWeapon(EWeaponType Weapon);
	
	// Interface for pickups
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckIfAmmoPickable(EWeaponType WType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckFreeSlotForWeapon(int32& FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckIfWeaponUnique(FName NewWeaponName);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void Server_SetWeaponToInventorySlot(AActor* PickUp, FWeaponSlot NewWeapon, int32 SlotId);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool ChangeWeaponAtInventorySlot(int32 SlotIndex, FWeaponSlot NewWeapon, int32 CurrentWeaponIndex, FDropItem& OutDropItemInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetDropItemInfo(int32 SlotIndex, FDropItem &OutInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void Server_DropWeaponByIndex(int32 Index);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
		void Server_InitInventory(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots);


	UFUNCTION(NetMulticast, Reliable)
		void Multicast_AmmoChangedEvent(EWeaponType WeaponType, int32 NewAmmoVal);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_WeaponAmmoEmptyEvent(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_WeaponAmmoAvailableEvent(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_InventorySlotsUpdateEvent(int32 SlotIndex, FWeaponSlot SlotInfo);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_WeaponClipLoadChangedEvent(int32 WeaponIndex, int32 NewClipLoadVal);
	UFUNCTION(Server, Reliable)
		void Server_SwitchWeaponEvent(FName WeaponIdName, FAdditionalWeaponInfo WeaponAddInfo, int32 WeaponIndex);
};
