// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDSHealthComponent.h"
#include "TDSCharacterHealth.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChanged, float, ShieldValue, float, Damage);

/**
 * 
 */
UCLASS()
class TDS_API UTDSCharacterHealth : public UTDSHealthComponent
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChanged OnShieldChanged;

	virtual void Server_ChangeCurrentHealth(float HealthOffset) override;

public:

	float GetShieldValue() const;
	void SetShieldValue(float NewValue);
	void ChangeShieldValue(float ShieldOffset);

protected:
	void StartShieldRecovery();
	void RepairShield();

protected:
	FTimerHandle ShieldCalldownTimer, ShieldRecoveryTimer;
	float ShieldValue = 100.0f;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldTimeToCalldown = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;


	UFUNCTION(NetMulticast, Reliable)
		void Multicast_ShieldChangedEvent(float newShieldValue, float Damage);
};
