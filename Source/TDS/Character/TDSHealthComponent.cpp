// Fill out your copyright notice in the Description page of Project Settings.

#include "TDSHealthComponent.h"
#include "Net/UnrealNetwork.h"

//USTRUCT(BlueprintType)
//struct FStatsParam
//{
//	GENERATED_BODY()
//};

// Sets default values
UTDSHealthComponent::UTDSHealthComponent()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}

// Called when the game starts or when spawned
void UTDSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTDSHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDSHealthComponent, Health);
	DOREPLIFETIME(UTDSHealthComponent, bIsAlive);
}

float UTDSHealthComponent::GetCurrentHealth() const
{
	return Health;
}

void UTDSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;

	if (Health < 0.0f) {
		Health = 0.0f;
		bIsAlive = false;
		Multicast_DeathEvent();
	}
	else if (Health > 100.0f) {
		Health = 100.0f;
	}
}

void UTDSHealthComponent::Server_ChangeCurrentHealth_Implementation(float HealthOffset)
{
	if (bIsAlive) {
		float NewHealth = 0.0f;
		float ApplyHealthOffset = 0.0f;

		if (HealthOffset < 0.0f) {
			ApplyHealthOffset = HealthOffset * DamageCoef;
		}
		else {
			ApplyHealthOffset = HealthOffset * HealCoef;
		}

		NewHealth = GetCurrentHealth() + ApplyHealthOffset;

		Multicast_HeathChangedEvent(NewHealth, ApplyHealthOffset);

		SetCurrentHealth(NewHealth);
	}
}

void UTDSHealthComponent::Multicast_DeathEvent_Implementation()
{
	OnDeathEvent.Broadcast();
}

void UTDSHealthComponent::Multicast_HeathChangedEvent_Implementation(float HealthOffset, float Damage)
{
	OnHealthChanged.Broadcast(HealthOffset, Damage);
}
