// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FunctionLibrary/Types.h"
#include "TDS/Weapon/WeaponDefault.h"
#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Character/TDSCharacterHealth.h"
#include "TDS/Interface/TDSUIGameActor.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDSUIGameActor
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }	

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDSCharacterHealth* HealthComponent;

public:

	// Tick movement function
	UFUNCTION()
		void MovementTick(float DeltaTime);
	// Start function
	UFUNCTION()
		void BeginPlay();

	virtual void SetupPlayerInputComponent(UInputComponent* InInputComponent) override;

	// Interface
	virtual EPhysicalSurface GetSurfaceType() override;
	virtual TArray<UTDSStateEffect*> GetAllCurrentEffects() override;
	virtual void RemoveEffect(UTDSStateEffect* RemoveEffect) override;
	virtual void AddEffect(UTDSStateEffect* NewEffect) override;
	virtual void GetSpawmEffectInfo(FName& AttachedBone, FVector& Offset) override;
	//bool AvailableForEffects_Implementation() override;
	// Interface End

	UFUNCTION()
		void CharacterDeath();
	UFUNCTION(BlueprintCallable)
		bool IsAlive() const;
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_PlayCharDeadAnim(UAnimMontage* Anim);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_EnableRagDoll();

	// read inputs
	UFUNCTION()
		void InputAxisX(float value);
	UFUNCTION()
		void InputAxisY(float value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION(BlueprintCallable)
		void InputReloadWeapon();
	UFUNCTION(BlueprintCallable)
		void InputSetNextWeapon();
	UFUNCTION(BlueprintCallable)
		void InputSetPrevWeapon();
	UFUNCTION(BlueprintCallable)
		void InputAbilityCast();
	UFUNCTION()
		void InputDropCurrentWeapon();


	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	UFUNCTION(BlueprintCallable)
		EMovementState GetMovementState() const;


	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName WeaponName, FAdditionalWeaponInfo WeaponAddInfo, int32 WeaponIndex);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon() const;

	// weapon events
	UFUNCTION()
		void WeaponFire();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFire_BP(UAnimMontage* Anim);

	UFUNCTION()
		void WeaponReloadStart();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	
	UFUNCTION()
		void WeaponReloadEnd(bool bSuccess, int32 AmmoTake);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bSuccess);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld() const;

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();


	UFUNCTION(Server, Unreliable)
		void Server_SetActorRotationByYaw(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void Multicast_SetActorRotationByYaw(float Yaw);
	UFUNCTION(Server, Reliable)
		void Server_SetMovementState(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_SetMovementState(EMovementState NewState);
	UFUNCTION(Server, Reliable)
		void Server_ReloadWeapon();
	UFUNCTION(Server, Reliable)
		void Server_SetNewWeaponInDirection(bool bForward);


	UFUNCTION()
		void Notify_AddEffect();
	UFUNCTION()
		void Notify_RemoveEffect();
	UFUNCTION()
		void SwitchEffect(UTDSStateEffect* Effect, bool bShouldAdd);
	UFUNCTION(Server, Reliable)
		void Server_ExecuteNewEffect(UParticleSystem* Effect);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_ExecuteNewEffect(UParticleSystem* Effect);
public:

	// Life
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Life")
	//	bool bAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Life")
		TArray<UAnimMontage*> DeathAnims;
	FTimerHandle RagDollTimer;

	// Cursor
	UDecalComponent* CurrentCursor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	// Movement
	UPROPERTY(Replicated)
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bSprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bWalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bAimEnabled = false;

	// Weapon
	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentWeaponIndex = 0;
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	// Effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDSStateEffect> AbilityEffect;
	UPROPERTY(Replicated)
		TArray<UTDSStateEffect*> CurrentEffects;
	UPROPERTY(ReplicatedUsing = Notify_AddEffect)
		UTDSStateEffect* RepAddedEffect = nullptr;
	UPROPERTY(ReplicatedUsing = Notify_RemoveEffect)
		UTDSStateEffect* RepRevomedEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> CurrentParticleCompoponents;

	// demo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;
	
	float AxisX = 0.0f;
	float AxisY = 0.0f;

};

