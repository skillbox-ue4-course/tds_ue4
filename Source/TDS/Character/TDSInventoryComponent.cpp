// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/Interface/TDSUIGameActor.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}

// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTDSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTDSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UTDSInventoryComponent, AmmoSlots);
}

void UTDSInventoryComponent::Server_InitInventory_Implementation(const TArray<FWeaponSlot>& NewWeaponSlots, const TArray<FAmmoSlot>& NewAmmoSlots)
{
	WeaponSlots = NewWeaponSlots;
	AmmoSlots = NewAmmoSlots;

	MaxSlotsNumber = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0)) {
		if (!WeaponSlots[0].NameItem.IsNone())
			Server_SwitchWeaponEvent(WeaponSlots[0].NameItem, WeaponSlots[0].AddInfo, 0);		//	OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AddInfo, 0);
	}
}

// set new weapon
bool UTDSInventoryComponent::SwitchWeaponToNext(bool bForward, int32 PrevIndex, FAdditionalWeaponInfo PrevInfo)
{
	bool bSuccess = false;

	FName NewWeaponName;
	FAdditionalWeaponInfo NewWeaponAddInfo;
	EWeaponType WeaponType = EWeaponType::Pistol;

	int8 resIndex = PrevIndex;

	// try to apply weapon with ammo
	for (int8 i = 0; i < WeaponSlots.Num(); ++i) {
		resIndex = bForward ? (resIndex + 1) : (resIndex - 1);

		if (resIndex > WeaponSlots.Num() - 1)
			resIndex = 0;
		else if (resIndex < 0)
			resIndex = WeaponSlots.Num() - 1;
		UE_LOG(LogTemp, Warning, TEXT("SwitchWeaponByIndex - NewIndex %i"), resIndex);

		int8 j = 0;

		while (j < WeaponSlots.Num() && !bSuccess) {
			if (j == resIndex) {
				if (!WeaponSlots[j].NameItem.IsNone() && (WeaponSlots[j].TypeItem != EWeaponType::None)) {
					NewWeaponName = WeaponSlots[j].NameItem;
					NewWeaponAddInfo = WeaponSlots[j].AddInfo;
					WeaponType = WeaponSlots[j].TypeItem;

					// check ammo for weapon
					if ((NewWeaponAddInfo.ClipLoad > 0) || (GetAvailableAmmoForWeapon(WeaponType) > 0) && (WeaponSlots[j].TypeItem != EWeaponType::None))
						bSuccess = true;
				}
			}
			++j;
		}

		UE_LOG(LogTemp, Warning, TEXT("SwitchWeaponByIndex - NewWeaponName %s, WeaponType %i"), *NewWeaponName.ToString(), WeaponType);

		if (bSuccess) {
			SetAdditionalWeaponInfoByIndex(PrevIndex, PrevInfo);
			Server_SwitchWeaponEvent(NewWeaponName, NewWeaponAddInfo, resIndex);
			//OnSwitchWeapon.Broadcast(NewWeaponName, NewWeaponAddInfo, resIndex);
			break;
		}
	}

	if (!bSuccess) {
		// TODO set default pistol
	}

	return bSuccess;
}

int32 UTDSInventoryComponent::GetWeaponIndexByName(FName WeaponName) const
{
	int32 res = -1;
	int8 i = 0;
	bool bWeaponFind = false;
	while (i < WeaponSlots.Num() && !bWeaponFind) {
		if (WeaponSlots[i].NameItem == WeaponName) {
			res = i;
			bWeaponFind = true;
		}
		++i;
	}

	return res;
}

void UTDSInventoryComponent::Server_DropWeaponByIndex_Implementation(int32 Index)
{
	FWeaponSlot EmptySlot;
	FDropItem OutInfo;

	bool bCanDrop = false;
	int8 i = 0;
	int8 WeaponNum = 0;
	while (i < WeaponSlots.Num() && !bCanDrop) {
		if (!WeaponSlots[i].NameItem.IsNone()) {
			WeaponNum++;
			if (WeaponNum > 1)
				bCanDrop = true;
		}
		i++;
	}

	if (bCanDrop && WeaponSlots.IsValidIndex(Index) && GetDropItemInfo(Index, OutInfo)) {
		// switch to valid weapon
		bool bWeaponFind = false;
		int8 j = 0;
		while (j < WeaponSlots.Num() && !bWeaponFind) {
			if (!WeaponSlots[j].NameItem.IsNone()) {
				Server_SwitchWeaponEvent(WeaponSlots[j].NameItem, WeaponSlots[j].AddInfo, j);
				//OnSwitchWeapon.Broadcast(WeaponSlots[i].NameItem, WeaponSlots[i].AddInfo, j);
			}
			j++;
		}
	}

	WeaponSlots[Index] = EmptySlot;
	if (GetOwner()->GetClass()->ImplementsInterface(UTDSUIGameActor::StaticClass())) {
		ITDSUIGameActor::Execute_DropWeaponToWorld(GetOwner(), OutInfo);
	}

	Multicast_InventorySlotsUpdateEvent(Index, EmptySlot);
	//OnInventorySlotsUpdate.Broadcast(Index, EmptySlot);
}

FName UTDSInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot) const
{
	if (WeaponSlots.IsValidIndex(IndexSlot))
		return WeaponSlots[IndexSlot].NameItem;

	return FName();
}

FAdditionalWeaponInfo UTDSInventoryComponent::GetAdditionalWeaponInfoByIndex(int32 IndexWeapon) const
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		return WeaponSlots[IndexWeapon].AddInfo;
	}
	return FAdditionalWeaponInfo();
}

void UTDSInventoryComponent::SetAdditionalWeaponInfoByIndex(int32 IndexWeapon, FAdditionalWeaponInfo AddInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bWeaponFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bWeaponFind) {
			if (i == IndexWeapon) {
				WeaponSlots[i].AddInfo = AddInfo;
				bWeaponFind = true;

				//UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeapon - ClipLoadVal %i"), AddInfo.ClipLoad);
				//OnWeaponClipLoadChanged.Broadcast(IndexWeapon, AddInfo.ClipLoad);
				Multicast_WeaponClipLoadChangedEvent(IndexWeapon, AddInfo.ClipLoad);
			}
			++i;
		}
		if (!bWeaponFind)
			UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeapon - No weapons with index %i"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("SetAdditionalWeapon - Index %i isn't correct"), IndexWeapon);
}

void UTDSInventoryComponent::ChangeAmmoSlotValue(EWeaponType Weapon, int32 AmmoDiff)
{
	bool bFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bFind) {
		if (AmmoSlots[i].Weapon == Weapon) {
			//UE_LOG(LogTemp, Warning, TEXT("WeaponAmmoNumberChanged - CurrAmmo %i, AmmoTaken %i"), AmmoSlots[i].CurrAmmo, AmmoTaken);
			AmmoSlots[i].CurrAmmo += AmmoDiff;
			if (AmmoSlots[i].CurrAmmo > AmmoSlots[i].MaxAmmo)
				AmmoSlots[i].CurrAmmo = AmmoSlots[i].MaxAmmo;

			//UE_LOG(LogTemp, Warning, TEXT("WeaponAmmoNumberChanged - NewVal %i"), AmmoSlots[i].CurrAmmo);
			// Test!!!
			//OnAmmoChanged.Broadcast(AmmoSlots[i].Weapon, AmmoSlots[i].CurrAmmo);
			Multicast_AmmoChangedEvent(AmmoSlots[i].Weapon, AmmoSlots[i].CurrAmmo);

			bFind = true;
		}
		++i;
	}
}

int32 UTDSInventoryComponent::GetAvailableAmmoForWeapon(EWeaponType Weapon)
{
	int32 res = 0;
	int8 i = 0;
	while (i < AmmoSlots.Num()) {
		if (AmmoSlots[i].Weapon == Weapon && AmmoSlots[i].CurrAmmo > 0) {
			res = AmmoSlots[i].CurrAmmo;
			break;
		}
		i++;
	}

	if (res == 0) {
		Multicast_WeaponAmmoEmptyEvent(Weapon);
		//OnWeaponAmmoEmpty.Broadcast(Weapon);
		UE_LOG(LogTemp, Warning, TEXT("GetAvailableAmmoForWeapon - No more ammo for weapon id %i"), Weapon);
	}

	return res;
}

bool UTDSInventoryComponent::CheckIfAmmoPickable(EWeaponType WType)
{
	bool res = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !res) {
		if ((AmmoSlots[i].Weapon == WType) && (AmmoSlots[i].CurrAmmo < AmmoSlots[i].MaxAmmo)) {
			res = true;
		}
		++i;
	}

	return res;
}

bool UTDSInventoryComponent::CheckFreeSlotForWeapon(int32& FreeSlot)
{
	bool res = false;
	int8 i = 0;

	while (i < WeaponSlots.Num() && !res) {
		if (WeaponSlots[i].NameItem.IsNone()) {
			res = true;
			FreeSlot = i;
		}
		++i;
	}

	return res;
}

bool UTDSInventoryComponent::CheckIfWeaponUnique(FName NewWeaponName)
{
	bool res = true;
	int8 i = 0;

	while (i < WeaponSlots.Num()) {
		if (WeaponSlots[i].NameItem == NewWeaponName) {
			res = false;
		}
		++i;
	}

	return res;
}

void UTDSInventoryComponent::Server_SetWeaponToInventorySlot_Implementation(AActor* PickUp, FWeaponSlot NewWeapon, int32 SlotId)
{
	//int32 freeIndex = -1;

	//bool res = false;
	//if (CheckFreeSlotForWeapon(freeIndex)) {
	//	if (WeaponSlots.IsValidIndex(freeIndex)) {
	//		WeaponSlots[freeIndex] = NewWeapon;
	//		res = true;

	//		Multicast_InventorySlotsUpdateEvent(freeIndex, NewWeapon);
	//		//OnInventorySlotsUpdate.Broadcast(freeIndex, NewWeapon);

	//		if (PickUp)
	//			PickUp->Destroy();
	//	}
	//}

	//return res;

	if (WeaponSlots[SlotId].TypeItem == EWeaponType::None) {
		WeaponSlots[SlotId] = NewWeapon;

		Multicast_InventorySlotsUpdateEvent(SlotId, NewWeapon);

		if (PickUp) {
			PickUp->Destroy();
		}
	}

}

bool UTDSInventoryComponent::ChangeWeaponAtInventorySlot(int32 SlotIndex, FWeaponSlot NewWeapon, int32 CurrentWeaponIndex, FDropItem& OutDropItemInfo)
{
	if (WeaponSlots.IsValidIndex(SlotIndex) && GetDropItemInfo(SlotIndex, OutDropItemInfo)) {
		WeaponSlots[SlotIndex] = NewWeapon;

		FName NewWeaponName = WeaponSlots[SlotIndex].NameItem;
		FAdditionalWeaponInfo NewWeaponAddInfo = WeaponSlots[SlotIndex].AddInfo;
		EWeaponType WeaponType = WeaponSlots[SlotIndex].TypeItem;

		UE_LOG(LogTemp, Warning, TEXT("ChangeWeaponAtInventorySlot - NewWeaponName %s, WeaponType %i"), *NewWeaponName.ToString(), WeaponType);

		SetAdditionalWeaponInfoByIndex(SlotIndex, NewWeaponAddInfo);
		Server_SwitchWeaponEvent(NewWeaponName, NewWeaponAddInfo, SlotIndex);
		//OnSwitchWeapon.Broadcast(NewWeaponName, NewWeaponAddInfo, SlotIndex);

		Multicast_InventorySlotsUpdateEvent(SlotIndex, NewWeapon);
		//OnInventorySlotsUpdate.Broadcast(SlotIndex, NewWeapon);
		return true;
	}

	return false;
}

bool UTDSInventoryComponent::GetDropItemInfo(int32 SlotIndex, FDropItem& OutInfo)
{
	bool res = false;
	FName DropItemName = GetWeaponNameBySlotIndex(SlotIndex);
	UTDSGameInstance* myInst = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (myInst) {
		if (myInst->GetDropItemByName(DropItemName, OutInfo)) {
			res = true;
			if (WeaponSlots.IsValidIndex(SlotIndex))
				OutInfo.WeaponInfo.AddInfo = WeaponSlots[SlotIndex].AddInfo;
		}
	}

	return res;
}

// NetMulticasts

void UTDSInventoryComponent::Multicast_AmmoChangedEvent_Implementation(EWeaponType WeaponType, int32 NewAmmoVal)
{
	OnAmmoChanged.Broadcast(WeaponType, NewAmmoVal);
}

void UTDSInventoryComponent::Multicast_WeaponAmmoEmptyEvent_Implementation(EWeaponType WeaponType)
{
	OnWeaponAmmoEmpty.Broadcast(WeaponType);
}

void UTDSInventoryComponent::Multicast_WeaponAmmoAvailableEvent_Implementation(EWeaponType WeaponType)
{
	OnWeaponAmmoAvailable.Broadcast(WeaponType);
}

void UTDSInventoryComponent::Multicast_InventorySlotsUpdateEvent_Implementation(int32 SlotIndex, FWeaponSlot SlotInfo)
{
	OnInventorySlotsUpdate.Broadcast(SlotIndex, SlotInfo);
}

void UTDSInventoryComponent::Multicast_WeaponClipLoadChangedEvent_Implementation(int32 WeaponIndex, int32 NewClipLoadVal)
{
	OnWeaponClipLoadChanged.Broadcast(WeaponIndex, NewClipLoadVal);
}

void UTDSInventoryComponent::Server_SwitchWeaponEvent_Implementation(FName WeaponIdName, FAdditionalWeaponInfo WeaponAddInfo, int32 WeaponIndex)
{
	OnSwitchWeapon.Broadcast(WeaponIdName, WeaponAddInfo, WeaponIndex);
}
