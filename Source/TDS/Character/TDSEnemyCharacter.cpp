// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Character/TDSEnemyCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATDSEnemyCharacter::ATDSEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATDSEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATDSEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ATDSEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


bool ATDSEnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	check(Channel);
	check(Bunch);
	check(RepFlags);

	bool WroteSomething = false;

	for (int32 i = 0; i < CurrentEffects.Num(); ++i) {
		if (CurrentEffects[i]) {
			WroteSomething |= Channel->ReplicateSubobject(CurrentEffects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return WroteSomething;
}

void ATDSEnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDSEnemyCharacter, CurrentEffects);
	DOREPLIFETIME(ATDSEnemyCharacter, RepRevomedEffect);
	DOREPLIFETIME(ATDSEnemyCharacter, RepAddedEffect);
}


void ATDSEnemyCharacter::RemoveEffect(UTDSStateEffect* RmEffect)
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSEnvStructure::RemoveEffect - %s"), *RmEffect->GetName());
	CurrentEffects.Remove(RmEffect);

	if (!RmEffect->bAvtoExpose) {
		SwitchEffect(RmEffect, false);
		RepRevomedEffect = RmEffect;
	}
}

void ATDSEnemyCharacter::AddEffect(UTDSStateEffect* NewEffect)
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSEnvStructure::AddEffect - %s"), *NewEffect->GetName());

	CurrentEffects.Add(NewEffect);
	if (NewEffect->bAvtoExpose) {
		if (NewEffect->ParticleEffect) {
			Server_ExecuteNewEffect(NewEffect->ParticleEffect);
		}
	}
	else {
		SwitchEffect(NewEffect, true);
		RepAddedEffect = NewEffect;
	}
}

void ATDSEnemyCharacter::GetSpawmEffectInfo(FName& AttachedBone, FVector& Offset)
{
	AttachedBone = FName("Spine2");
	Offset = FVector(5.0f, 5.0f, 5.0f);
}

void ATDSEnemyCharacter::Notify_AddEffect()
{
	if (RepAddedEffect) {
		SwitchEffect(RepAddedEffect, true);
	}
}

void ATDSEnemyCharacter::Notify_RemoveEffect()
{
	if (RepRevomedEffect) {
		SwitchEffect(RepAddedEffect, false);
	}
}

void ATDSEnemyCharacter::SwitchEffect(UTDSStateEffect* Effect, bool bShouldAdd)
{
	if (bShouldAdd) {
		if (Effect && Effect->ParticleEffect) {

			FName AttachedBone = Effect->BoneName;
			FVector Loc = FVector::ZeroVector + Effect->Offset;

			USceneComponent* mesh = GetMesh();

			if (mesh) {
				UParticleSystemComponent* ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, mesh, FName(AttachedBone),
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

				if (ParticleEmitter) {
					CurrentParticleCompoponents.Add(ParticleEmitter);
					UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::SwitchEffect: ParticleEmitter - %s at Bone - %s"),
						*ParticleEmitter->GetFName().ToString(), *AttachedBone.ToString())
				}
			}
		}
	}
	else {
		int32 i = 0;
		bool bFind = false;
		while (i < CurrentParticleCompoponents.Num() && !bFind) {
			if (CurrentParticleCompoponents[i]->Template && Effect->ParticleEffect && (Effect->ParticleEffect == CurrentParticleCompoponents[i]->Template)) {
				bFind = true;
				CurrentParticleCompoponents[i]->DeactivateSystem();
				CurrentParticleCompoponents[i]->DestroyComponent();
				CurrentParticleCompoponents.RemoveAt(i);
			}
			++i;
		}
	}
}

void ATDSEnemyCharacter::Server_ExecuteNewEffect_Implementation(UParticleSystem* Effect)
{
	Multicast_ExecuteNewEffect(Effect);
}

void ATDSEnemyCharacter::Multicast_ExecuteNewEffect_Implementation(UParticleSystem* Effect)
{
	FName AttachedName;
	FVector Offset;
	GetSpawmEffectInfo(AttachedName, Offset);
	UTypes::ExecuteNewEffect(this, Effect, Offset, AttachedName);
}




