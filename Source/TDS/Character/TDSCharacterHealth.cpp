// Fill out your copyright notice in the Description page of Project Settings.

#include "TDSCharacterHealth.h"

void UTDSCharacterHealth::Server_ChangeCurrentHealth(float HealthOffset)
{
	float ApplyHealthDiff = HealthOffset;
	// take damage
	if (HealthOffset < 0.0f) {
		// try block with shield
		if (ShieldValue > 0.0f) {
			ChangeShieldValue(HealthOffset * DamageCoef);
			ApplyHealthDiff = 0.0f;
		}
	}

	Super::Server_ChangeCurrentHealth(ApplyHealthDiff);
}

float UTDSCharacterHealth::GetShieldValue() const
{
	return ShieldValue;
}

void UTDSCharacterHealth::SetShieldValue(float NewValue)
{
	ShieldValue = NewValue;
}

void UTDSCharacterHealth::ChangeShieldValue(float ShieldOffset)
{
	float NewShieldVal = GetShieldValue() + ShieldOffset;

	if (NewShieldVal > 100.0f)
		SetShieldValue(100.0f);
	else if (NewShieldVal < 0.0f)
		SetShieldValue(0.0f);
	else
		SetShieldValue(NewShieldVal);

	// multicast
	Multicast_ShieldChangedEvent(GetShieldValue(), ShieldOffset);
	//OnShieldChanged.Broadcast(GetShieldValue(), ShieldOffset);

	if (GetWorld()) {
		//GetWorld()->GetTimerManager().ClearTimer(ShieldCalldownTimer);
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this); // stop calldown and recovery
		GetWorld()->GetTimerManager().SetTimer(ShieldCalldownTimer, this, &UTDSCharacterHealth::StartShieldRecovery, ShieldTimeToCalldown, false);
	}
}

void UTDSCharacterHealth::StartShieldRecovery()
{
	if (GetWorld())
		GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryTimer, this, &UTDSCharacterHealth::RepairShield, ShieldRecoveryRate, true);
}

void UTDSCharacterHealth::RepairShield()
{
	float NewShieldVal = GetShieldValue() + ShieldRecoveryValue;
	float ShieldValRealOffset = ShieldRecoveryValue;

	if (NewShieldVal > 100.0f) {
		if (GetWorld())
			GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryTimer);
		ShieldValRealOffset = 100.0f - GetShieldValue();
		SetShieldValue(100.0f);
	}
	else {
		SetShieldValue(NewShieldVal);
	}

	// multicast
	Multicast_ShieldChangedEvent(GetShieldValue(), ShieldValRealOffset);
	//OnShieldChanged.Broadcast(GetShieldValue(), ShieldValRealOffset);
}

void UTDSCharacterHealth::Multicast_ShieldChangedEvent_Implementation(float newShieldValue, float Damage)
{
	OnShieldChanged.Broadcast(newShieldValue, Damage);
}
