// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDSHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, HealthOffset, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathEvent);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_API UTDSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	UTDSHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnHealthChanged OnHealthChanged;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnDeathEvent OnDeathEvent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
		float Health = 100.f;
	UPROPERTY(Replicated)
		bool bIsAlive = true;

public:

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float DamageCoef = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float HealCoef = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetCurrentHealth() const;
	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(float NewHealth);
	UFUNCTION(BlueprintCallable, Category = "Health")
	FORCEINLINE	bool IsAlive() const {
		return bIsAlive;
	};

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Health")
		virtual void Server_ChangeCurrentHealth(float HealthOffset);

	UFUNCTION(NetMulticast, Reliable)
		void Multicast_HeathChangedEvent(float HealthOffset, float Damage);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_DeathEvent();
	
};
