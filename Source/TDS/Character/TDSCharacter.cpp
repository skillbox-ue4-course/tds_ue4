// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

#include "TDS/Game/TDSPlayerController.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/Weapon/ProjectileDefault.h"
#include "TDS/TDS.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Inventory
	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent) {
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}

	// Health
	HealthComponent = CreateDefaultSubobject<UTDSCharacterHealth>(TEXT("HealthComponent"));
	if (HealthComponent) {
		HealthComponent->OnDeathEvent.AddDynamic(this, &ATDSCharacter::CharacterDeath);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Network
	bReplicates = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (IsAlive()) {
		if (CurrentCursor) {
			APlayerController* myPC = Cast<APlayerController>(GetController());
			if (myPC && myPC->IsLocalPlayerController()) {
				FHitResult TraceHitResult;
				myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
				FVector CursorFV = TraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();

				CurrentCursor->SetWorldLocation(TraceHitResult.Location);
				CurrentCursor->SetWorldRotation(CursorR);
			}
		}

		MovementTick(DeltaSeconds);
	}
}

float ATDSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (IsAlive()) {
		HealthComponent->Server_ChangeCurrentHealth(-ActualDamage);

		if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID)) {
			AProjectileDefault* Projectile = Cast<AProjectileDefault>(DamageCauser);
			if (Projectile) {
				UTypes::AddEffectBySurfaceType(this, Projectile->ProjectileSettings.Effect, GetSurfaceType());
			}

		}
	}

	return ActualDamage;
}

void ATDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATDSCharacter, MovementState);
	DOREPLIFETIME(ATDSCharacter, CurrentWeapon);
	DOREPLIFETIME(ATDSCharacter, CurrentWeaponIndex);
	DOREPLIFETIME(ATDSCharacter, CurrentEffects);
	DOREPLIFETIME(ATDSCharacter, RepRevomedEffect);
	DOREPLIFETIME(ATDSCharacter, RepAddedEffect);
}

bool ATDSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	check(Channel);
	check(Bunch);
	check(RepFlags);

	bool WroteSomething = false;

	for (int32 i = 0; i < CurrentEffects.Num(); ++i) {
		if (CurrentEffects[i]) 	{
			WroteSomething |= Channel->ReplicateSubobject(CurrentEffects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return WroteSomething;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer) {
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority) {
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchToNextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputSetNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchToPrevWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputSetPrevWeapon);
	NewInputComponent->BindAction(TEXT("AbilityCast"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAbilityCast);

	NewInputComponent->BindAction(TEXT("DropWeaponAction"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputDropCurrentWeapon);
}

EPhysicalSurface ATDSCharacter::GetSurfaceType()
{
	EPhysicalSurface ResSurface = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponent) {
		if (HealthComponent->GetShieldValue() <= 0) {
			if (GetMesh()) {
				UMaterialInterface* Material = GetMesh()->GetMaterial(0);
				if (Material) {
					ResSurface = Material->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return ResSurface;
}

TArray<UTDSStateEffect*> ATDSCharacter::GetAllCurrentEffects()
{
	return CurrentEffects;
}

void ATDSCharacter::RemoveEffect(UTDSStateEffect* Effect)
{
	CurrentEffects.Remove(Effect);

	if (!Effect->bAvtoExpose) {
		SwitchEffect(Effect, false);
		RepRevomedEffect = Effect;
	}
}

void ATDSCharacter::AddEffect(UTDSStateEffect* Effect)
{
	CurrentEffects.Add(Effect);
	if (Effect->bAvtoExpose) {
		if (Effect->ParticleEffect) {
			Server_ExecuteNewEffect(Effect->ParticleEffect);
		}
	}
	else {
		SwitchEffect(Effect, true);
		RepAddedEffect = Effect;
	}
	
}

void ATDSCharacter::Notify_AddEffect()
{
	if (RepAddedEffect)	{
		SwitchEffect(RepAddedEffect, true);
	}
}

void ATDSCharacter::Notify_RemoveEffect()
{
	if (RepRevomedEffect) {
		SwitchEffect(RepRevomedEffect, false);
	}
}

void ATDSCharacter::SwitchEffect(UTDSStateEffect* Effect, bool bShouldAdd)
{
	if (bShouldAdd) {
		if (Effect && Effect->ParticleEffect) {
			FName AttachedBone = Effect->BoneName;
			FVector Loc = FVector::ZeroVector + Effect->Offset;

			USceneComponent* mesh = GetMesh();

			if (mesh) {
				UParticleSystemComponent* ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, mesh, FName(AttachedBone),
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

				if (ParticleEmitter) {
					CurrentParticleCompoponents.Add(ParticleEmitter);
					UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::SwitchEffect: ParticleEmitter - %s at Bone - %s"), 
						*ParticleEmitter->GetFName().ToString(), *AttachedBone.ToString())
				}
			}
		}
	}
	else {
		int32 i = 0;
		bool bFind = false;
		while (i < CurrentParticleCompoponents.Num() && !bFind) {
			if (CurrentParticleCompoponents[i]->Template && Effect->ParticleEffect && (Effect->ParticleEffect == CurrentParticleCompoponents[i]->Template)) {
				bFind = true;
				CurrentParticleCompoponents[i]->DeactivateSystem();
				CurrentParticleCompoponents[i]->DestroyComponent();
				CurrentParticleCompoponents.RemoveAt(i);
			}
			++i;
		}
	}
}

void ATDSCharacter::Server_ExecuteNewEffect_Implementation(UParticleSystem* Effect)
{
	Multicast_ExecuteNewEffect(Effect);
}

void ATDSCharacter::Multicast_ExecuteNewEffect_Implementation(UParticleSystem* Effect)
{
	UTypes::ExecuteNewEffect(this, Effect, FVector(0), FName("spine_01"));
}

void ATDSCharacter::GetSpawmEffectInfo(FName& AttachedBone, FVector& Offset)
{
	AttachedBone = FName("spine_02");

	Offset = FVector(5.0f, 5.0f, 5.0f);
}

void ATDSCharacter::CharacterDeath()
{
	CharDead_BP();

	if (HasAuthority()) {

		if (GetController()) {
			GetController()->UnPossess();
		}

		float AnimPlayTime = 0.0f;
		int8 rnd = FMath::RandHelper(DeathAnims.Num());
		if (DeathAnims.IsValidIndex(rnd) && DeathAnims[rnd]) {
			UAnimMontage* Anim = DeathAnims[rnd];
			AnimPlayTime = Anim->GetPlayLength();

			if (GetMesh()) {
				Multicast_PlayCharDeadAnim(Anim);
			}
		}

		//bAlive = false;
		GetCharacterMovement()->DisableMovement();
		SetLifeSpan(10.0f);

		if (GetCurrentWeapon()) {
			AttackCharEvent(false);
			GetCurrentWeapon()->Server_SetWeaponState(false); // disable firing
			if (GetCurrentWeapon()->bWeaponReloading) {
				GetCurrentWeapon()->StopReloading();
			}

			GetCurrentWeapon()->SetLifeSpan(10.0f);
		}

		// TODO timer ragdoll
		GetWorldTimerManager().SetTimer(RagDollTimer, this, &ATDSCharacter::Multicast_EnableRagDoll, AnimPlayTime / 2, false);
	}
	else {
		if (GetCursorToWorld()) {
			GetCursorToWorld()->SetVisibility(false);
		}
	}

	if (GetCapsuleComponent()) {
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
	}

	
	
}

bool ATDSCharacter::IsAlive() const
{
	return HealthComponent && HealthComponent->IsAlive();
}

void ATDSCharacter::Multicast_PlayCharDeadAnim_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance()) {
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void ATDSCharacter::Multicast_EnableRagDoll_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::Multicast_EnableRagDoll - Dead :("));

	if (GetMesh()) {
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

void ATDSCharacter::CharDead_BP_Implementation()
{
	// in BP
}

void ATDSCharacter::InputAxisX(float value)
{
	AxisX = value;
}

void ATDSCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATDSCharacter::InputAttackPressed()
{
	if (IsAlive()) {
		AttackCharEvent(true);
	}
}

void ATDSCharacter::InputAttackReleased()
{
	if (IsAlive()) {
		AttackCharEvent(false);
	}
}

void ATDSCharacter::InputAbilityCast()
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InputAbilityCast"));
	if (AbilityEffect) {
		UTDSStateEffect* NewEffect = NewObject<UTDSStateEffect>(this, AbilityEffect);
		if (NewEffect) {
			NewEffect->InitObject(this);
		}
	}
}

void ATDSCharacter::InputDropCurrentWeapon()
{
	if (InventoryComponent) {
		FDropItem ItemInfo;
		InventoryComponent->Server_DropWeaponByIndex(CurrentWeaponIndex);
	}
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	if (!(GetController() && GetController()->IsLocalPlayerController())) {
		return;
	}

	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	//FString SEnum = UEnum::GetValueAsString(MovementState);
	//UE_LOG(LogTDS_Net, Warning, TEXT("Movement State - %s"), *SEnum);

	if (MovementState == EMovementState::SpeedRun_State) {
		FVector rotationVector = FVector(AxisX, AxisY, 0.0f);
		FRotator rotator = rotationVector.ToOrientationRotator();

		SetActorRotation((FQuat(rotator)));
		Server_SetActorRotationByYaw(rotator.Yaw);
	}
	else {
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController) {
			FHitResult ResultHit;
			//MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, ResultHit);

			FRotator CharacterLook = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location);
			float NewYaw = CharacterLook.Yaw;

			SetActorRotation(FQuat(FRotator(0.0f, NewYaw, 0.0f)));
			Server_SetActorRotationByYaw(NewYaw);

			if (CurrentWeapon) {
				FVector aimDisplacement = FVector(0.0f, 0.0f, 170.0f); // weapon offset above Y axis while aiming
				FVector baseDisplacement = FVector(0.0f, 0.0f, 130.0f); // weapon offset above Y axis while not aiming
				FVector displacement = FVector(0);
				bool bReduce = false;
				switch (MovementState) {
				case EMovementState::Aim_State: case EMovementState::AimedWalk_State:
					displacement = aimDisplacement;
					bReduce = true;
					break;
				case EMovementState::Walk_State: case EMovementState::Run_State:
					displacement = baseDisplacement;
					bReduce = false;
					break;
				case EMovementState::SpeedRun_State:
					break;
				default:
					break;
				}

				//CurrentWeapon->ShootEndLocation = ResultHit.Location + displacement;
				CurrentWeapon->Server_SetWeaponShootEndLocation(ResultHit.Location + displacement, bReduce);
			}
		}
	}
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState) {
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.Walk_Speed;
		break;
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.Aim_Speed;
		break;
	case EMovementState::AimedWalk_State:
		ResSpeed = MovementInfo.AimedWalk_Speed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.Run_Speed;
		break;
	case EMovementState::SpeedRun_State:
		ResSpeed = MovementInfo.SpeedRun_Speed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (!(GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)) {
		return;
	}

	EMovementState NewState = EMovementState::Walk_State;

	if (bSprintRunEnabled && !bAimEnabled) {
		NewState = EMovementState::SpeedRun_State;
	}
	else if (bSprintRunEnabled && bAimEnabled) {
		NewState = EMovementState::Aim_State;
	}
	else if (bWalkEnabled && !bAimEnabled) {
		NewState = EMovementState::Walk_State;
	}
	else if (bWalkEnabled && bAimEnabled) {
		NewState = EMovementState::AimedWalk_State;
	}
	else if (!bWalkEnabled && bAimEnabled) {
		NewState = EMovementState::Aim_State;
	}
	else {
		NewState = EMovementState::Run_State;
	}

	//FString SRole = UEnum::GetValueAsString(GetRemoteRole());
	//FString SEnum = UEnum::GetValueAsString(NewState);
	//UE_LOG(LogTDS_Net, Warning, TEXT("ChangeMovementState: Movement State at %s - %s"), *SRole, *SEnum);

	//CharacterUpdate();
	Server_SetMovementState(NewState);

	// weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon) {
		myWeapon->Server_UpdateStateWeapon(NewState);
	}
}

EMovementState ATDSCharacter::GetMovementState() const
{
	return MovementState;
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* muWeapon = GetCurrentWeapon();
	if (muWeapon) {
		muWeapon->Server_SetWeaponState(bIsFiring);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::AttackCharEvent - No current weapon"));
	}
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void ATDSCharacter::InitWeapon(FName WeaponName, FAdditionalWeaponInfo WeaponAddInfo, int32 WeaponIndex)
{
	// on server

	if (CurrentWeapon) {
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo WeaponInfo;
	if (myGI) {
		if (myGI->GetWeaponInfoByName(WeaponName, WeaponInfo)) {
			if (WeaponInfo.WeaponClass) {
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotaion = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* weapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotaion, SpawnParams));
				if (weapon) {
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					weapon->AttachToComponent(GetMesh(), Rule, FName("WeaponRightHandSocket"));
					CurrentWeapon = weapon;
					CurrentWeapon->WeaponIdName = WeaponName;

					weapon->SetWeaponSettings(WeaponInfo);
					weapon->Server_UpdateStateWeapon(MovementState);

					weapon->WeaponAdditionalInfo = WeaponAddInfo;
					if (InventoryComponent) {
						//CurrentWeaponIndex = InventoryComponent->GetWeaponIndexByName(WeaponName);
						CurrentWeaponIndex = WeaponIndex;
					}

					weapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
					weapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
					weapon->OnWeaponFire.AddDynamic(this, &ATDSCharacter::WeaponFire);

					// reload if needed after switch
					if (weapon->GetCurrentWeaponClipLoad() <= 0 && weapon->CanWeaponBeReloaded()) {
						weapon->InitWeaponReload();
					}

					if (InventoryComponent) {
						InventoryComponent->Multicast_WeaponAmmoAvailableEvent(weapon->WeaponSettings.WeaponType); 
						// OnWeaponAmmoAvailable.Broadcast(weapon->WeaponSettings.WeaponType);
					}
				}
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - No such weapon %s in table"), *WeaponName.ToString());
		}
	}
}

void ATDSCharacter::InputReloadWeapon()
{
	if (CurrentWeapon && IsAlive() && !CurrentWeapon->bWeaponReloading) {
		Server_ReloadWeapon();
	}
}

void ATDSCharacter::InputSetNextWeapon()
{
	Server_SetNewWeaponInDirection(true);
}

void ATDSCharacter::InputSetPrevWeapon()
{
	Server_SetNewWeaponInDirection(false);
}

void ATDSCharacter::WeaponFire()
{
	if (InventoryComponent && CurrentWeapon) {
		InventoryComponent->SetAdditionalWeaponInfoByIndex(CurrentWeaponIndex, CurrentWeapon->WeaponAdditionalInfo);
	}

	if (bAimEnabled) {
		WeaponFire_BP(CurrentWeapon->WeaponSettings.AnimCharAimedFire);
	}
	else {
		WeaponFire_BP(CurrentWeapon->WeaponSettings.AnimCharFire);
	}
}

void ATDSCharacter::WeaponFire_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDSCharacter::WeaponReloadStart()
{
	if (bAimEnabled) {
		WeaponReloadStart_BP(CurrentWeapon->WeaponSettings.AnimCharAimedReload);
	}
	else {
		WeaponReloadStart_BP(CurrentWeapon->WeaponSettings.AnimCharReload);
	}
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDSCharacter::WeaponReloadEnd(bool bSuccess, int32 AmmoTake)
{
	//bReloadEnabled = false;
	if (InventoryComponent && CurrentWeapon) {
		InventoryComponent->ChangeAmmoSlotValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
	}
	WeaponReloadEnd_BP(bSuccess);

	if (InventoryComponent && CurrentWeapon) {
		InventoryComponent->SetAdditionalWeaponInfoByIndex(CurrentWeaponIndex, CurrentWeapon->WeaponAdditionalInfo);
	}
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bSuccess)
{
	// in BP
}

UDecalComponent* ATDSCharacter::GetCursorToWorld() const
{
	return CurrentCursor;
}

/*
*  Client - Server Logic
*/

void ATDSCharacter::Server_SetActorRotationByYaw_Implementation(float Yaw)
{
	Multicast_SetActorRotationByYaw(Yaw);
}

void ATDSCharacter::Multicast_SetActorRotationByYaw_Implementation(float Yaw)
{
	if (Controller) {
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATDSCharacter::Server_SetMovementState_Implementation(EMovementState NewState)
{
	Multicast_SetMovementState(NewState);
}

void ATDSCharacter::Multicast_SetMovementState_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ATDSCharacter::Server_ReloadWeapon_Implementation()
{
	if (CurrentWeapon->GetCurrentWeaponClipLoad() < CurrentWeapon->WeaponSettings.MaxClipLoad && CurrentWeapon->CanWeaponBeReloaded()) {
		CurrentWeapon->InitWeaponReload();
	}
}

void ATDSCharacter::Server_SetNewWeaponInDirection_Implementation(bool bForward)
{
	if (InventoryComponent->WeaponSlots.Num() > 1) {
		int8 prevWeaponIndex = CurrentWeaponIndex;
		FAdditionalWeaponInfo prevWeaponInfo;
		if (CurrentWeapon) {
			prevWeaponInfo = CurrentWeapon->WeaponAdditionalInfo;

			if (CurrentWeapon->bWeaponReloading) {
				CurrentWeapon->StopReloading();
			}
		}

		if (InventoryComponent) {
			if (InventoryComponent->SwitchWeaponToNext(bForward, prevWeaponIndex, prevWeaponInfo)) {
			}
		}
	}
}