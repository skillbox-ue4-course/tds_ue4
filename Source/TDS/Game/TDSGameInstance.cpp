// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Game/TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& Out)
{
	bool res = false;
	if (WeaponInfoTable) {
		FWeaponInfo* WeaponInfo = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "", false);
		if (WeaponInfo) {
			res = true;
			Out = *WeaponInfo;
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDSCharacter::GetWeaponInfoByName - WeponTable is NULL"));
	}
	

	return res;
}

bool UTDSGameInstance::GetDropItemByName(FName ItemName, FDropItem& Out)
{
	bool res = false;
	
	if (DropItemTable) {
		FDropItem* DropItem;
		TArray<FName> RowNames = DropItemTable->GetRowNames();

		int8 i = 0;
		while (i < RowNames.Num() && !res) {
			DropItem = DropItemTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItem->WeaponInfo.NameItem == ItemName) {
				Out = *DropItem;
				res = true;
			}
			++i;
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetDropItemByName - DropTable is NULL"));
	}


	return res;
}
