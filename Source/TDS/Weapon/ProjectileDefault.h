// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "TDS/FunctionLibrary/Types.h"
#include "ProjectileDefault.generated.h"

UCLASS()
class TDS_API AProjectileDefault : public AActor
{
	GENERATED_BODY()

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* BulletMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UProjectileMovementComponent* BulletProjectileMovement = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UParticleSystemComponent* BulletFX = nullptr;

public:
	// Sets default values for this actor's properties
	AProjectileDefault();

	FProjectileinfo ProjectileSettings;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;

	UFUNCTION(BlueprintCallable)
		void InitProjectile(FProjectileinfo Settings);

	UFUNCTION()
		void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
		void BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		virtual	void ImpactProjectile();

	UFUNCTION(NetMulticast, Reliable)
		void Multicast_InitMesh(UStaticMesh* Mesh, FTransform RelativeTransform);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_InitTrail(UParticleSystem* Trail, FTransform RelativeTransform);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_SpawnHitDecal(UMaterialInterface* HitDecal, UPrimitiveComponent* OtherComp, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_SpawnHitFX(UParticleSystem* HitFX, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_SpawnHitSound(USoundBase* HitSound, FHitResult HitResult);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_InitBulletSpeed(float InitSpeed, float MaxSpeed);
};
