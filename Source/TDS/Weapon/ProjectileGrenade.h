// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS/Weapon/ProjectileDefault.h"
#include "ProjectileGrenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AProjectileGrenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ExploseTick(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	virtual void ImpactProjectile() override;

	UFUNCTION(NetMulticast, Reliable)
		void Multicast_PostProcessGrenade(UParticleSystem* FireFX, USoundBase* FireSound);

	void Explose();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GrenadeSet")
		bool bTimerEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GrenadeSet")
		float ExploseRate = 2.0f;
	
	float TimerToExplose = 0.0f;
};
