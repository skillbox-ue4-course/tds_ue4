// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDS/FunctionLibrary/Types.h"
#include "TDS/Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bSuccess, int32, AmmoTake);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFire);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponFire OnWeaponFire;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;


	FName WeaponIdName;
	UPROPERTY()
		FWeaponInfo WeaponSettings;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
		FAdditionalWeaponInfo WeaponAdditionalInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool bWeaponFiring = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		bool bWeaponReloading = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		float FireRate = 0.0f;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
		float ReloadRate = 0.0f;

	// Remove, just for debugging 
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic Debug")
		float ReloadTime = 0.0f;

	bool bBlockFire = false;
	// dispersion
	UPROPERTY(Replicated)
		bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	UPROPERTY(Replicated)
		FVector ShootEndLocation = FVector(0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool bShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// drop item logic
	bool bDropSleeveNeeded = false;
	bool bDropClipNeeded = false;
	float SleeveDropTimer = 0.0f;
	float ClipDropTimer = 0.0f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void SleevesDropTick(float DeltaTime);
	void EmtryMagDropTick(float DeltaTime);

	void WeaponInit();
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
		void Server_SetWeaponState(bool bNewFireState);
	UFUNCTION(Server, Reliable)
		void Server_UpdateStateWeapon(EMovementState NewMovementState);
	UFUNCTION(Server, Reliable)
		void Server_SetWeaponShootEndLocation(FVector EndShootLocation, bool bRebuceDispersion);
	UFUNCTION(Server, Reliable, BlueprintCallable)
		void Server_InitDropItem(FDropWeaponItem DropItem);
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
		void Multicast_InitDropItem(FDropWeaponItem DropItem);
	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
		void Multicast_InitWeaponFX(UParticleSystem* FireFX, USoundBase* FireSound);

	UFUNCTION(NetMulticast, Unreliable, BlueprintCallable)
		void Multicast_PostProcessProjectileTrace(UMaterialInterface* HitDecal, UParticleSystem* FireFX, USoundBase* FireSound, FHitResult Hit);
	
	void SetWeaponSettings(FWeaponInfo settings);
	UFUNCTION(BlueprintCallable)
		FWeaponInfo GetWeaponSettings() const;

	FProjectileinfo GetProjectile() const;

	void Fire();
	bool CheckWeaponCanFire();

	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;

	UFUNCTION(BlueprintCallable)
		int32 GetCurrentWeaponClipLoad() const;
	

	bool CanWeaponBeReloaded();
	int32 GetAvailableClipLoadVal();
	void InitWeaponReload();
	void StopReloading();
	void ReloadFinished();
	
};
