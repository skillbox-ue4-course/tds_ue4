// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapon/ProjectileGrenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplose"),
	DebugExplodeShow,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat
);

void AProjectileGrenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileGrenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ExploseTick(DeltaTime);
}

void AProjectileGrenade::ExploseTick(float DeltaTime)
{
	//UE_LOG(LogTemp, Warning, TEXT("Grenade timer %i: %f"), bTimerEnabled, TimerToExplose);
	/*if (bTimerEnabled) {
		if (TimerToExplose > ExploseRate) {
			Explose();
		}
		else {
			TimerToExplose += DeltaTime;
		}
	}*/
}

void AProjectileGrenade::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileGrenade::ImpactProjectile()
{
	/*bTimerEnabled = true;*/
	Explose();
}

void AProjectileGrenade::Multicast_PostProcessGrenade_Implementation(UParticleSystem* ExplodeFX, USoundBase* ExplodeSound)
{
	if (ExplodeFX) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplodeFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}

	if (ExplodeSound) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplodeSound, GetActorLocation());
	}
}

void AProjectileGrenade::Explose()
{

	if (DebugExplodeShow) {
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.MinExplodeRadius, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.MaxExplodeRadius, 12, FColor::Red, false, 12.0f);
	}

	bTimerEnabled = false;

	UParticleSystem* ExParticle = nullptr;
	USoundBase* ExSound = nullptr;

	if (ProjectileSettings.ExploseFX) {
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		ExParticle = ProjectileSettings.ExploseFX;
	}

	if (ProjectileSettings.ExploseSound) {
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExploseSound, GetActorLocation());
		ExSound = ProjectileSettings.ExploseSound;
	}

	Multicast_PostProcessGrenade(ExParticle, ExSound);

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		ProjectileSettings.ExploseMaxDamage,
		ProjectileSettings.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		100.0f,
		200.0f,
		5,
		NULL, IgnoredActor, this, nullptr
	);

	this->Destroy();
}
