// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "../Character/TDSInventoryComponent.h"
#include "TDSStateEffect.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create scene component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	// create weapon skeletal mesh
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	// create weapon static mesh
	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	// create shoot location
	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Shoot Location"));
	ShootLocation->SetupAttachment(RootComponent);

	// network
	bReplicates = true;
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AWeaponDefault, WeaponAdditionalInfo);
	DOREPLIFETIME(AWeaponDefault, bWeaponReloading);
	DOREPLIFETIME(AWeaponDefault, ReloadRate);
	DOREPLIFETIME(AWeaponDefault, ReloadTime);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		//DispersionTick(DeltaTime);
		SleevesDropTick(DeltaTime);
		EmtryMagDropTick(DeltaTime);
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (bWeaponFiring && GetCurrentWeaponClipLoad() > 0) {
		if (FireRate < 0.0f) {
			if (!bWeaponReloading)
				Fire();
		}
		else
			FireRate -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (bWeaponReloading) {
		if (ReloadRate < 0.0f) {
			ReloadFinished();
			UE_LOG(LogTemp, Warning, TEXT("Reload Process Finished"));
		}
		else {
			ReloadRate -= DeltaTime;
			if (bShowDebug)
				UE_LOG(LogTemp, Warning, TEXT("ReloadWeaponProcess: %f"), ReloadRate);
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!bWeaponReloading)
	{
		if (!bWeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	/*if (bShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);*/
}

void AWeaponDefault::SleevesDropTick(float DeltaTime)
{
	if (bDropSleeveNeeded) {
		if (SleeveDropTimer < 0.0f) {
			Server_InitDropItem(WeaponSettings.SleeveDrop);
			bDropSleeveNeeded = false;
			//UE_LOG(LogTemp, Warning, TEXT("DropSleeve"));
		}
		else {
			SleeveDropTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::EmtryMagDropTick(float DeltaTime)
{
	if (bDropClipNeeded) {
		//UE_LOG(LogTemp, Warning, TEXT("Time to drop clip: %f"), ClipDropTimer);
		if (ClipDropTimer < 0.0f) {
			Server_InitDropItem(WeaponSettings.EmptyClipDrop);
			bDropClipNeeded = false;
			//UE_LOG(LogTemp, Warning, TEXT("DropMag"));
		}
		else {
			ClipDropTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) {
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh()) {
		StaticMeshWeapon->DestroyComponent();
	}

	Server_UpdateStateWeapon(EMovementState::Run_State);
}

void AWeaponDefault::Server_SetWeaponState_Implementation(bool bNewFireState)
{
	if (CheckWeaponCanFire()) {
		bWeaponFiring = bNewFireState;
	}
	else {
		bWeaponFiring = false;
	}
	FireRate = 0.01f;//!!!!!
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !bBlockFire;
}

void AWeaponDefault::Server_SetWeaponShootEndLocation_Implementation(FVector EndShootLocation, bool bRebuceDispersion)
{
	ShootEndLocation = EndShootLocation;
	ShouldReduceDispersion = bRebuceDispersion;
}

void AWeaponDefault::Multicast_PostProcessProjectileTrace_Implementation(UMaterialInterface* HitDecal, UParticleSystem* FireFX, USoundBase* FireSound, FHitResult Hit)
{
	if (HitDecal) {
		UGameplayStatics::SpawnDecalAttached(HitDecal, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
	}
	
	if (FireFX) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireFX, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
	}

	if (FireSound) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), FireSound, Hit.ImpactPoint);
	}
}

void AWeaponDefault::SetWeaponSettings(FWeaponInfo settings)
{
	WeaponSettings = settings;
	WeaponAdditionalInfo.ClipLoad = WeaponSettings.MaxClipLoad;
	ReloadRate = WeaponSettings.ReloadTime;
	// for DEBUG !!!
	ReloadTime = WeaponSettings.ReloadTime;
}

FWeaponInfo AWeaponDefault::GetWeaponSettings() const
{
	return WeaponSettings;
}

FProjectileinfo AWeaponDefault::GetProjectile() const
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	FireRate = WeaponSettings.RateOfFire;
	--WeaponAdditionalInfo.ClipLoad;
	//UE_LOG(LogTemp, Warning, TEXT("Fire - ClipLoad %i"), WeaponAdditionalInfo.ClipLoad);
	//ChangeDispersionByShot();

	Multicast_InitWeaponFX(WeaponSettings.FXWeaponFire, WeaponSettings.SoundWeaponFire);

	int8 numOfProjectile = GetNumberProjectileByShot();

	if (ShootLocation) {
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileinfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < numOfProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire
				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);
				}

				// drop sleeves
				if (WeaponSettings.SleeveDrop.DropMesh) {
					SleeveDropTimer = WeaponSettings.SleeveDrop.SpawnTimeDelay;
					bDropSleeveNeeded = true;
				}
			}
			else {
				// Projectile null Init trace fire

				/* TODO Multicast */

				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSettings.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors, EDrawDebugTrace::ForDuration, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

				if (bShowDebug) {
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSettings.DistanceTrace, FColor::Black, false, 5.0f, (uint8)'\000', 0.5f);
				}

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid()) {
					
					EPhysicalSurface mySurface = UGameplayStatics::GetSurfaceType(Hit);
					UMaterialInterface* myMaterial = nullptr;
					UParticleSystem* myParticle = nullptr;
					USoundBase* mySound = nullptr;

					if (WeaponSettings.ProjectileSettings.HitDecals.Contains(mySurface)) {
						myMaterial = WeaponSettings.ProjectileSettings.HitDecals[mySurface];
						/*if (myMaterial && Hit.GetComponent()) {
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
						}*/
					}

					if (WeaponSettings.ProjectileSettings.HitParticles.Contains(mySurface))
					{
						myParticle = WeaponSettings.ProjectileSettings.HitParticles[mySurface];
						/*if (myParticle) {
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}*/
					}
					
					if (WeaponSettings.ProjectileSettings.HitSound) {
						//UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.ProjectileSettings.HitSound, Hit.ImpactPoint);
						mySound = WeaponSettings.ProjectileSettings.HitSound;
					}

					Multicast_PostProcessProjectileTrace(myMaterial, myParticle, mySound, Hit);
					
					UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurface);	

					UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSettings.ProjectileSettings.ProjectileDamage, GetInstigatorController(), this, NULL);
				}
			}
		}
	}

	// anim send to character
	if (WeaponSettings.AnimCharFire)
		OnWeaponFire.Broadcast();

	// check reload
	if (!bWeaponReloading && GetCurrentWeaponClipLoad() <= 0) {
		if (CanWeaponBeReloaded())
			InitWeaponReload();
	}
}

void AWeaponDefault::Server_UpdateStateWeapon_Implementation(EMovementState NewMovementState)
{
	//ChangeDispersionByShot();
	bBlockFire = false;

	switch (NewMovementState) {
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Aim_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Aim_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Aim_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Aim_DispersionReduction;
		break;
	case EMovementState::AimedWalk_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.AimWalk_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.AimWalk_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.AimWalk_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Aim_DispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Walk_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Walk_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Walk_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Aim_DispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSettings.WeaponDispersion.Run_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.WeaponDispersion.Run_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.WeaponDispersion.Run_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.WeaponDispersion.Aim_DispersionReduction;
		break;
	case EMovementState::SpeedRun_State:
		bBlockFire = true;
		Server_SetWeaponState(false);//set fire trigger to false
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	//return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);
	return DirectionShoot;
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector endLocation = FVector(0.0f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic) {
		endLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (bShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		endLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (bShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (bShowDebug) {
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), endLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}

	return endLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

int32 AWeaponDefault::GetCurrentWeaponClipLoad() const
{
	return WeaponAdditionalInfo.ClipLoad;
}


void AWeaponDefault::Multicast_InitWeaponFX_Implementation(UParticleSystem* FireFX, USoundBase* FireSound)
{
	if (FireSound)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, ShootLocation->GetComponentLocation());
	if (FireFX)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireFX, ShootLocation->GetComponentLocation());
}


void AWeaponDefault::Server_InitDropItem_Implementation(FDropWeaponItem DropItem)
{
	Multicast_InitDropItem(DropItem);
}

void AWeaponDefault::Multicast_InitDropItem_Implementation(FDropWeaponItem DropItem)
{
	if (DropItem.DropMesh) {

		// Spawn Coords
		FTransform newTransform;
		FVector LocalDir = this->GetActorForwardVector() * DropItem.LocationOffset.GetLocation().X +
			this->GetActorForwardVector() * DropItem.LocationOffset.GetLocation().Y + this->GetActorForwardVector() * DropItem.LocationOffset.GetLocation().Z;

		newTransform.SetLocation(GetActorLocation() + LocalDir);
		newTransform.SetRotation((GetActorRotation() + DropItem.LocationOffset.Rotator()).Quaternion());
		newTransform.SetScale3D(DropItem.LocationOffset.GetScale3D());

		AStaticMeshActor* newActor = nullptr;
		FActorSpawnParameters newParams;
		newParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		newParams.Owner = this;
		newActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), newTransform, newParams);
		//newActor->SetReplicates(true);

		if (newActor && newActor->GetStaticMeshComponent()) {
			newActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			newActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			newActor->SetActorTickEnabled(false);
			newActor->InitialLifeSpan = DropItem.LifeTime;

			newActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			newActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			newActor->GetStaticMeshComponent()->SetStaticMesh(DropItem.DropMesh);

			newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			newActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (DropItem.CustomMass > 0.0f)
				newActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, DropItem.CustomMass, true);

			if (!DropItem.ImpulseDirection.IsNearlyZero()) {
				FVector finaleDir;
				LocalDir += (DropItem.ImpulseDirection * 1000.f);

				if (!FMath::IsNearlyZero(DropItem.ImpulseRandomDispersion)) {
					finaleDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, DropItem.ImpulseRandomDispersion);
				}

				finaleDir.GetSafeNormal(0.0001f);
				newActor->GetStaticMeshComponent()->AddImpulse(finaleDir * DropItem.ImpulsePower);
			}
		}
	}
}

bool AWeaponDefault::CanWeaponBeReloaded()
{
	bool res = false;
	if (GetOwner()) {
		UTDSInventoryComponent* inventory = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (inventory) {
			if (inventory->GetAvailableAmmoForWeapon(WeaponSettings.WeaponType) > 0)
				res = true;
		}
	}


	return res;
}

int32 AWeaponDefault::GetAvailableClipLoadVal()
{
	int32 res = WeaponSettings.MaxClipLoad;
	if (GetOwner()) {
		UTDSInventoryComponent* inventory = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (inventory) {
			int32 availableAmmo = inventory->GetAvailableAmmoForWeapon(WeaponSettings.WeaponType);
			if (availableAmmo > WeaponSettings.MaxClipLoad)
				res = WeaponSettings.MaxClipLoad;
			else
				res = availableAmmo;
		}
	}
	return res;
}

void AWeaponDefault::InitWeaponReload()
{
	bWeaponReloading = true;
	ReloadRate = WeaponSettings.ReloadTime;

	// drop empty clip
	if (WeaponSettings.EmptyClipDrop.DropMesh) {
		ClipDropTimer = WeaponSettings.EmptyClipDrop.SpawnTimeDelay;
		bDropClipNeeded = true;
	}

	if (WeaponSettings.AnimCharReload)
		OnWeaponReloadStart.Broadcast();
}

void AWeaponDefault::StopReloading()
{
	bWeaponReloading = false;
	bDropClipNeeded = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	OnWeaponReloadEnd.Broadcast(false, 0);
}

void AWeaponDefault::ReloadFinished()
{
	int32 AvailableLoad = GetAvailableClipLoadVal(); // 0..max_ClipLoad
	int32 NewClipLoad = GetCurrentWeaponClipLoad() + GetAvailableClipLoadVal(); // 0..(max_ClipLoad+curr_ClipLoad)
	// normalize
	if (NewClipLoad > WeaponSettings.MaxClipLoad)
		NewClipLoad = WeaponSettings.MaxClipLoad;

	int32 AmmoTaken = WeaponAdditionalInfo.ClipLoad - NewClipLoad; // AmmoTaken < 0

	UE_LOG(LogTemp, Warning, TEXT("ReloadFinished - CurrAmmoVal %i, MaxAmmoVal %i, AmmoTaken %i"), GetCurrentWeaponClipLoad(), WeaponSettings.MaxClipLoad, -AmmoTaken);
	
	bWeaponReloading = false;
	WeaponAdditionalInfo.ClipLoad = NewClipLoad;
	OnWeaponReloadEnd.Broadcast(true, AmmoTaken);
}