// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStateEffect.h"

#include "TDS/Character/TDSHealthComponent.h"
#include "TDS/Interface/TDSUIGameActor.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

bool UTDSStateEffect::InitObject(AActor* Actor)
{
	bool res = false;
	
	mActor = Actor;

	ITDSUIGameActor* UI = Cast<ITDSUIGameActor>(mActor);
	if (UI) {
		UI->GetSpawmEffectInfo(BoneName, Offset);
	}
	else {
		// for enemy char
		BoneName = FName("Spine2");
		Offset = FVector(5.0f, 5.0f, 5.0f);
	}

	UE_LOG(LogTemp, Warning, TEXT("UTDSStateEffect::InitObject: Actor - %s, Effect - %s"), *mActor->GetName(), *this->GetName());
	//ITDSUIGameActor* UI = Cast<ITDSUIGameActor>(mActor);
	if (UI)	{
		UI->AddEffect(this);
		res = true;
	}

	return res;
}

void UTDSStateEffect::DestroyObject()
{
	ITDSUIGameActor* UI = Cast<ITDSUIGameActor>(mActor);
	UE_LOG(LogTemp, Warning, TEXT("UTDSStateEffect::DestroyObject: Actor - %s, Effect - %s"), *mActor->GetName(), *this->GetName());
	if (UI)	{
		UI->RemoveEffect(this);
	}

	mActor = nullptr;

	if (this && this->IsValidLowLevel()) {
		this->ConditionalBeginDestroy();
	}
}

void UTDSStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UTDSStateEffect, BoneName);
	DOREPLIFETIME(UTDSStateEffect, Offset);
}


bool UTDStateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	if (Actor) {
		Super::InitObject(Actor);
		Execute();
		return true;
	}
	else {
		return false;
	}
}

void UTDStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDStateEffect_ExecuteOnce::Execute()
{
	if (mActor)	{
		UTDSHealthComponent* Health = Cast<UTDSHealthComponent>(mActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (Health) {
			Health->Server_ChangeCurrentHealth(Power);
		}
	}
	DestroyObject();
}


bool UTDStateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(EffectTimer, this, &UTDStateEffect_ExecuteTimer::DestroyObject, TimeToDestroy, false);
	GetWorld()->GetTimerManager().SetTimer(ExecuteTimer, this, &UTDStateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect) {

		//UE_LOG(LogTemp, Warning, TEXT("UTDStateEffect_ExecuteTimer::InitObject: AttachedBone - %s, Transform - %s"), *AttachedBone.ToString(), *Offset.ToString());

		//USceneComponent* mesh = Cast<USceneComponent>(mActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		//if (mesh)
		//{
		//	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, mesh, FName(AttachedBone),
		//		FVector::ZeroVector + Offset, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		//}
		//else
		//{
		//	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, mActor->GetRootComponent(), FName(AttachedBone),
		//		FVector::ZeroVector + Offset, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		//}

		//
		//if (ParticleEmitter)
		//	UE_LOG(LogTemp, Warning, TEXT("UTDStateEffect_ExecuteTimer::InitObject: ParticleEmitter - %s"), *ParticleEmitter->GetFName().ToString());
	}

	return true;
}

void UTDStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld()) {
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}


	if (ParticleEmitter) {
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}
	Super::DestroyObject();
}

void UTDStateEffect_ExecuteTimer::Execute()
{
	if (mActor) {
		UTDSHealthComponent* Health = Cast<UTDSHealthComponent>(mActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (Health) {
			Health->Server_ChangeCurrentHealth(Power);
		}
	}
}
