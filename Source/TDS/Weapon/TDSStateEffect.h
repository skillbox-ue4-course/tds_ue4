// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TDSStateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDSStateEffect : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Surfaces")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurfaces;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bStackable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		bool bAvtoExpose = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | ExecuteTimer")
		UParticleSystem* ParticleEffect = nullptr;

	AActor* mActor = nullptr;

	UPROPERTY(Replicated)
		FName BoneName;
	UPROPERTY(Replicated)
		FVector Offset = FVector(0);

public:
	virtual bool InitObject(AActor* Actor);
	virtual void DestroyObject();
	FORCEINLINE virtual bool IsSupportedForNetworking() const override 
	{
		return true;
	};
	virtual void GetLifetimeReplicatedProps(TArray< class FLifetimeProperty >& OutLifetimeProps) const override;

};

UCLASS()
class TDS_API UTDStateEffect_ExecuteOnce : public UTDSStateEffect
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		float Power = 20.0f;

public:
	virtual bool InitObject(AActor* Actor) override;
	virtual void DestroyObject() override;

	virtual void Execute();
};

UCLASS()
class TDS_API UTDStateEffect_ExecuteTimer : public UTDSStateEffect
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		float TimeToDestroy = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		float RateTime = 1.0f;

public:
	virtual bool InitObject(AActor* Actor) override;
	virtual void DestroyObject() override;

	void Execute();

private:
	FTimerHandle ExecuteTimer;
	FTimerHandle EffectTimer;

	UParticleSystemComponent* ParticleEmitter = nullptr;

};