// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectileDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Perception/AISense_Damage.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial

	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	//BulletProjectileMovement->InitialSpeed = 1.f;
	//BulletProjectileMovement->MaxSpeed = 0.f;
	BulletProjectileMovement->ProjectileGravityScale = 0.1f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;

	bReplicates = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Custom projectile movement
	FVector ForwardVec = UKismetMathLibrary::GetForwardVector(BulletCollisionSphere->GetRelativeRotation());
	BulletProjectileMovement->Velocity = BulletProjectileMovement->Velocity + ForwardVec * BulletProjectileMovement->InitialSpeed;
	BulletProjectileMovement->UpdateComponentVelocity();
}

void AProjectileDefault::InitProjectile(FProjectileinfo Settings)
{
	ProjectileSettings = Settings;

	//BulletProjectileMovement->InitialSpeed = ProjectileSettings.ProjectileInitSpeed;
	//BulletProjectileMovement->MaxSpeed = ProjectileSettings.ProjectileMaxSpeed;
	this->SetLifeSpan(ProjectileSettings.ProjectileLifeTime);

	if (ProjectileSettings.ProjectileStaticMesh) {
		Multicast_InitMesh(ProjectileSettings.ProjectileStaticMesh, ProjectileSettings.ProjectileStaticMeshOffset);
	}
	else
		BulletMesh->DestroyComponent();


	if (ProjectileSettings.ProjectileTrailFx) {
		Multicast_InitTrail(ProjectileSettings.ProjectileTrailFx, ProjectileSettings.ProjectileTrailOffset);
	}
	else
		BulletFX->DestroyComponent();

	Multicast_InitBulletSpeed(ProjectileSettings.ProjectileInitSpeed, ProjectileSettings.ProjectileMaxSpeed);
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSettings.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileSettings.HitDecals[mySurfacetype];

			if (myMaterial && OtherComp)
			{
				Multicast_SpawnHitDecal(myMaterial, OtherComp, Hit);
			}
		}

		if (ProjectileSettings.HitParticles.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSettings.HitParticles[mySurfacetype];
			if (myParticle)
			{
				Multicast_SpawnHitFX(myParticle, Hit);
			}
		}

		if (ProjectileSettings.HitSound)
		{
			Multicast_SpawnHitSound(ProjectileSettings.HitSound, Hit);
		}

		UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileSettings.Effect, mySurfacetype);
	}

	UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSettings.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSettings.ProjectileDamage, Hit.Location, Hit.Location);
	ImpactProjectile();
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}

void AProjectileDefault::Multicast_InitBulletSpeed_Implementation(float InitSpeed, float MaxSpeed)
{
	if (BulletProjectileMovement) {
		BulletProjectileMovement->Velocity = GetActorForwardVector() * InitSpeed;
		BulletProjectileMovement->MaxSpeed = MaxSpeed;
	}
}

void AProjectileDefault::Multicast_SpawnHitDecal_Implementation(UMaterialInterface* HitDecal, UPrimitiveComponent* OtherComp, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(HitDecal, FVector(20.0f), OtherComp, NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AProjectileDefault::Multicast_SpawnHitFX_Implementation(UParticleSystem* HitFX, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
}

void AProjectileDefault::Multicast_SpawnHitSound_Implementation(USoundBase* HitSound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
}

void AProjectileDefault::Multicast_InitMesh_Implementation(UStaticMesh* Mesh, FTransform RelativeTransform)
{
	BulletMesh->SetStaticMesh(Mesh);
	BulletMesh->SetRelativeTransform(RelativeTransform);
}

void AProjectileDefault::Multicast_InitTrail_Implementation(UParticleSystem* Trail, FTransform RelativeTransform)
{
	BulletFX->SetTemplate(Trail);
	BulletFX->SetRelativeTransform(RelativeTransform);
}


void AProjectileDefault::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if (BulletProjectileMovement) {
		BulletProjectileMovement->Velocity = NewVelocity;
	}
}