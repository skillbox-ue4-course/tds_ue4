// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"

#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

#include "TDS/TDS.h"
#include "TDS/Weapon/TDSStateEffect.h"
#include "TDS/Interface/TDSUIGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* Owner, TSubclassOf<UTDSStateEffect> EffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && Owner && EffectClass)
	{
		UTDSStateEffect* Effect = Cast<UTDSStateEffect>(EffectClass->GetDefaultObject());
		if (Effect)
		{
			bool bPossibleSurface = false;
			int8 i = 0;
			while (i < Effect->PossibleInteractSurfaces.Num() && !bPossibleSurface)
			{
				if (Effect->PossibleInteractSurfaces[i] == SurfaceType)
				{
					bPossibleSurface = true;
					bool bCanAddEffect = false;
					if (!Effect->bStackable)
					{
						TArray<UTDSStateEffect*> CurEffects;
						ITDSUIGameActor* UI = Cast<ITDSUIGameActor>(Owner);
						if (UI)
						{
							CurEffects = UI->GetAllCurrentEffects();
						}

						if (CurEffects.Num() > 0)
						{
							int8 j = 0;
							while(j < CurEffects.Num() && !bCanAddEffect)
							{
								if (CurEffects[j]->GetClass() != EffectClass)
									bCanAddEffect = true;

								j++;
							}
						}
						else
							bCanAddEffect = true;
					}
					else
						bCanAddEffect = true;

					if (bCanAddEffect)
					{
						UTDSStateEffect* NewEffect = NewObject<UTDSStateEffect>(Owner, EffectClass);
						if (NewEffect)
							NewEffect->InitObject(Owner);
					}
					
				}
				++i;
			}
		}
	
	}
}

void UTypes::ExecuteNewEffect(AActor* Target, UParticleSystem* EffectFX, FVector Offset, FName Socket)
{
	if (Target && EffectFX) {
		FName AttachedBone = Socket;
		FVector Loc = Offset;

		ACharacter* Char = Cast<ACharacter>(Target);
		
		USceneComponent* mesh = Char->GetMesh();

		if (mesh) {
			UGameplayStatics::SpawnEmitterAttached(EffectFX, mesh, FName(AttachedBone),
				Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else {
			if (Target->GetRootComponent()) {
				UGameplayStatics::SpawnEmitterAttached(EffectFX, Target->GetRootComponent(), FName(AttachedBone),
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
}
