// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnvStructure.h"

#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

//#include "TDS/Weapon/TDSStateEffect.h"

// Sets default values
ATDSEnvStructure::ATDSEnvStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

// Called when the game starts or when spawned
void ATDSEnvStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDSEnvStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATDSEnvStructure::GetSurfaceType()
{
	EPhysicalSurface ResSurface = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* Mesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (Mesh)
	{
		UMaterialInterface* Material = Mesh->GetMaterial(0);
		if (Material)
		{
			ResSurface = Material->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return ResSurface;
}

TArray<UTDSStateEffect*> ATDSEnvStructure::GetAllCurrentEffects()
{
	return CurrentEffects;
}

void ATDSEnvStructure::RemoveEffect(UTDSStateEffect* RmEffect)
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSEnvStructure::RemoveEffect - %s"), *RmEffect->GetName());

	CurrentEffects.Remove(RmEffect);

	if (!RmEffect->bAvtoExpose) {
		SwitchEffect(RmEffect, false);
		RepRevomedEffect = RmEffect;
	}
}

void ATDSEnvStructure::AddEffect(UTDSStateEffect* NewEffect)
{
	UE_LOG(LogTemp, Warning, TEXT("ATDSEnvStructure::AddEffect - %s"), *NewEffect->GetName());

	CurrentEffects.Add(NewEffect);
	if (NewEffect->bAvtoExpose) {
		if (NewEffect->ParticleEffect) {
			Server_ExecuteNewEffect(NewEffect->ParticleEffect);
		}
	}
	else {
		SwitchEffect(NewEffect, true);
		RepAddedEffect = NewEffect;
	}
}

void ATDSEnvStructure::Notify_AddEffect()
{
	if (RepAddedEffect) {
		SwitchEffect(RepAddedEffect, true);
	}
}

void ATDSEnvStructure::Notify_RemoveEffect()
{
	if (RepRevomedEffect) {
		SwitchEffect(RepRevomedEffect, false);
	}
}

void ATDSEnvStructure::SwitchEffect(UTDSStateEffect* Effect, bool bShouldAdd)
{
	if (bShouldAdd) {
		if (Effect && Effect->ParticleEffect) {
			FName AttachedBone = NAME_None;
			FVector Loc = Offset;

			USceneComponent* RootComp = GetRootComponent();

			if (RootComp) {
				UParticleSystemComponent* ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, RootComp, FName(AttachedBone),
					Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

				if (ParticleEmitter) {
					CurrentParticleCompoponents.Add(ParticleEmitter);
					UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::SwitchEffect: ParticleEmitter - %s at Bone - %s"),
						*ParticleEmitter->GetFName().ToString(), *AttachedBone.ToString())
				}
			}
		}
	}
	else {
		int32 i = 0;
		bool bFind = false;
		while (i < CurrentParticleCompoponents.Num() && !bFind) {
			if (CurrentParticleCompoponents[i]->Template && Effect->ParticleEffect && (Effect->ParticleEffect == CurrentParticleCompoponents[i]->Template)) {
				bFind = true;
				CurrentParticleCompoponents[i]->DeactivateSystem();
				CurrentParticleCompoponents[i]->DestroyComponent();
				CurrentParticleCompoponents.RemoveAt(i);
			}
			++i;
		}
	}
}

void ATDSEnvStructure::Server_ExecuteNewEffect_Implementation(UParticleSystem* Effect)
{
	Multicast_ExecuteNewEffect(Effect);
}

void ATDSEnvStructure::Multicast_ExecuteNewEffect_Implementation(UParticleSystem* Effect)
{
	UTypes::ExecuteNewEffect(this, Effect, Offset, NAME_None);
}

void ATDSEnvStructure::GetSpawmEffectInfo(FName& OutAttachedBone, FVector& OutOffset)
{
	
	OutOffset = Offset;
	OutAttachedBone = NAME_None;
}

void ATDSEnvStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ATDSEnvStructure, CurrentEffects);
	DOREPLIFETIME(ATDSEnvStructure, RepRevomedEffect);
	DOREPLIFETIME(ATDSEnvStructure, RepAddedEffect);
}

bool ATDSEnvStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	check(Channel);
	check(Bunch);
	check(RepFlags);

	bool WroteSomething = false;

	for (int32 i = 0; i < CurrentEffects.Num(); ++i) {
		if (CurrentEffects[i]) {
			WroteSomething |= Channel->ReplicateSubobject(CurrentEffects[i], *Bunch, *RepFlags);	// (this makes those subobjects 'supported', and from here on those objects may have reference replicated)		
		}
	}
	return WroteSomething;
}

