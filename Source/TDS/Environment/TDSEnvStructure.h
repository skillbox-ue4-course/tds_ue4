// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/Interface/TDSUIGameActor.h"
#include "TDS/Weapon/TDSStateEffect.h"
#include "TDSEnvStructure.generated.h"

UCLASS()
class TDS_API ATDSEnvStructure : public AActor, public ITDSUIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSEnvStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual bool ReplicateSubobjects(class UActorChannel* Channel, class FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Interface
	virtual EPhysicalSurface GetSurfaceType() override;
	virtual TArray<UTDSStateEffect*> GetAllCurrentEffects() override;
	virtual void RemoveEffect(UTDSStateEffect* RemoveEffect) override;
	virtual void AddEffect(UTDSStateEffect* NewEffect) override;
	virtual void GetSpawmEffectInfo(FName& AttachedBone, FVector& Offset) override;

	UFUNCTION()
		void Notify_AddEffect();
	UFUNCTION()
		void Notify_RemoveEffect();
	UFUNCTION()
		void SwitchEffect(UTDSStateEffect* Effect, bool bShouldAdd);
	UFUNCTION(Server, Reliable)
		void Server_ExecuteNewEffect(UParticleSystem* Effect);
	UFUNCTION(NetMulticast, Reliable)
		void Multicast_ExecuteNewEffect(UParticleSystem* Effect);

public:

	// Effects
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Settings")
		TArray<UTDSStateEffect*> CurrentEffects;
	UPROPERTY(ReplicatedUsing = Notify_AddEffect)
		UTDSStateEffect* RepAddedEffect = nullptr;
	UPROPERTY(ReplicatedUsing = Notify_RemoveEffect)
		UTDSStateEffect* RepRevomedEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		TArray<UParticleSystemComponent*> CurrentParticleCompoponents;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		FVector Offset = FVector(0);
};
