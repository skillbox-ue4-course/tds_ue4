// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS/Weapon/TDSStateEffect.h"
#include "TDS/FunctionLibrary/Types.h"
#include "TDSUIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDSUIGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ITDSUIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UTDSStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDSStateEffect* RemoveEffect) = 0;
	virtual void AddEffect(UTDSStateEffect* NewEffect) = 0;
	virtual void GetSpawmEffectInfo(FName& AttachedBone, FVector& Offset) = 0;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void DropWeaponToWorld(FDropItem DropItemInfo);
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void DropAmmoToWorld(EWeaponType Type, int32 Count);
};
