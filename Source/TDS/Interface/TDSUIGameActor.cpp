// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSUIGameActor.h"

// Add default functionality here for any ITDSUIGameActor functions that are not pure virtual.

EPhysicalSurface ITDSUIGameActor::GetSurfaceType() 
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDSStateEffect*> ITDSUIGameActor::GetAllCurrentEffects()
{
	return TArray<UTDSStateEffect*>();
}
